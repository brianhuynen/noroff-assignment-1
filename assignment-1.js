class BankAccount
{
    // Init private fields
    #onLoan;

    // Bank account constructor, upon creation,
    // the balance and loan should be zero and
    // there should be no outstanding loans.
    constructor(name)
    {
        this.name = name;
        this.#onLoan = false;
        this.balance = 0;
        this.loanAmount = 0;
    }

    // Returns the name bound to the BankAccount
    getName()
    {
        return this.name;
    }
    // Returns the balance of the BankAccount
    getBalance()
    {
        return this.balance;
    }
    // Returns the value of the outstanding loan
    getLoanAmount()
    {
        return this.loanAmount;
    }
    // Checks whether there is an outstanding loan
    isOnLoan()
    {
        return this.#onLoan;
    }
    // Deposits an amount
    deposit(amount)
    {
        let amt = Number(amount);
        // console.log(`Depositing ${amt} EUR`);
        this.balance += amt;
    }
    // Withdraws an amount
    withdraw(amount)
    {
        let amt = Number(amount);
        // console.log(`Withdrawn ${amt} EUR`);
        this.balance -= amt;
    }
    // Loans amount
    loan(amount)
    {
        // Checks whether the amount you want to loan does not 
        // exceed double the current balance or whether there
        // already is an outstanding loan.
        let amt = Number(amount); //Ensure the amount is a number

        if(amt == 0)
        {
            throw new Error("Loaned amount is zero"); //return error when the loaned amount is zero
        }
        else if(amt > 2 * this.balance)
        {
            throw new Error("Loaned amount is too high"); //return error when the loaned amount is larger than twice the balance
        }
        else
        {
            this.balance += amt;
            this.loanAmount += amt;
            this.#onLoan = true;
        }
    }
    //returns the amount over the outstanding loan. Negative excess returns zero
    getExcess(amount)
    {
        let excess = amount - this.getLoanAmount();

        if(excess > 0)
            return excess;
        else
            return 0;
    }    
    // Payback an amount you loaned
    payLoan(amount)
    {
        let excess = this.getExcess(amount);
        if(amount >= this.loanAmount)
        {
            // If the paid back amount is too big or equal to the loaned amount,
            // just pay the loaned amount. The excess is returned to the balance.
            this.balance -= this.loanAmount;
            this.balance += excess;
            this.loanAmount = 0;
            this.#onLoan = false; //When loan is paid back, you are no longer on loan

            alert("Loan has been paid off!");
        }
        else{
            this.balance -= amount;
            this.loanAmount -= amount;

            alert(`Paid ${amount} EUR. Outstanding Loan: ${this.loanAmount} EUR`);
        }
    }
}

class Work
{
    //Instantiates a private constant since salary is a fixed amount
    #SALARY;
    //Work account constructor
    constructor(account)
    {
        this.bankAcc = account;
        this.#SALARY = 100;
        this.totalSalary = 0;
    }

    getSalary()
    {
        return this.totalSalary;
    }

    work()
    {
        this.totalSalary += this.#SALARY;
    }

    depositSalary()
    {
        let acc = this.bankAcc;
        let openLoan = acc.getLoanAmount(); //The amount of outstanding loans
        let finalSalary = this.getSalary(); //The final amount deposited to your bank account

        //If on loan, put aside part of the salary
        if(this.bankAcc.isOnLoan())
        {   
            let loanPay = 0.1 * this.totalSalary;
            //Check whether the part of the salary is larger than the outstanding
            //loan.
            let finalPay = (loanPay > openLoan) ? openLoan : loanPay;
            //If higher, the final payment should be equal to the outstanding loan
            //If lower, you pay part of the outstanding loan with the fraction of
            //your salary loanPay.
            acc.payLoan(finalPay);
            finalSalary -= finalPay;
        }

        acc.deposit(finalSalary);
        this.totalSalary = 0;
        return finalSalary; //For display purposes
    }
}

//initialize bank and work accounts
const bankAcc = new BankAccount('Brian Huynen');
const workAcc = new Work(bankAcc);

//initialize webpage elements
const laptopsElement = document.getElementById("laptops");
const buyElement = document.getElementById("buy-button");

//initialize database variables
const laptopPrice = document.getElementById("laptop-price");
const laptopDescription = document.getElementById("laptop-desc");
const laptopSpecs = document.getElementById("laptop-specs");
const laptopStock = document.getElementById("stock");
const laptopImg = document.getElementById("laptop-img");
const apiPath = "https://noroff-komputer-store-api.herokuapp.com/";

let laptops = []; //array for fetch function that will contain the info of each laptop
let onLoan = false; //variable that will be checked whether there is an outstanding loan or not

fetch(`${apiPath}computers`)
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

const addLaptopsToMenu = (laptops) =>
{
    laptops.forEach(x => addLaptopToMenu(x));
    laptopsInit(laptops);
}
// initialisation function upon opening the webpage
function laptopsInit(laptops)
{
    const initLaptop = laptops[0];
    laptopPrice.innerHTML = initLaptop.price;
    laptopDescription.innerHTML = initLaptop.description;
    laptopSpecs.innerHTML = listSpecs(initLaptop.specs); 
    laptopStock.innerHTML = initLaptop.stock;
    
    const path = apiPath + initLaptop.image;
    laptopImg.src = path;
    laptopImg.height = 200;
    laptopImg.width = 200;
}
//adds the laptops to the dropdown menu on the webpage
const addLaptopToMenu = (laptop) =>
{
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}
//updates the stock of the selected laptop
const updateLaptopStock = (laptop) =>
{
    laptop.stock--;
    laptopStock.innerHTML = laptop.stock;
}
// handles the events of the laptop selector, e.g. displays the required information
const laptopMenuHandler = e => 
{
    const selectedLaptop = laptops[e.target.selectedIndex];
    laptopPrice.innerHTML = selectedLaptop.price;
    laptopDescription.innerHTML = selectedLaptop.description;
    laptopSpecs.innerHTML = listSpecs(selectedLaptop.specs); 
    laptopStock.innerHTML = selectedLaptop.stock;
    
    const path = apiPath + selectedLaptop.image;
    laptopImg.src = path;
    laptopImg.height = 200;
    laptopImg.width = 200;
}
// if loading an image returns an error, try changing the file-extension from .jpg to .png
// and vice-versa
function fix()
{
    const extension = laptopImg.src.split('.');
    let img = laptopImg.src;
    if(extension.at(-1) === 'png') //Swap .png with .jpg
    {
        img = img.replace('.png', '.jpg');
    }
    else if (extension.at(-1) === 'jpg') //Swap .jpg with .png
    {
        img = img.replace('.jpg', '.png');
    }
    laptopImg.src = img;
}
//For loop to display each specs on a separate line
function listSpecs(input)
{
    let output = "";
    for(specs in input)
    {
        output += input[specs] + "<br>";
    }
    return output;
}
//Logic to handle when the Buy Now button is pressed
const handleBuyButton = () =>
{
    const selectedLaptop = laptops[laptopsElement.selectedIndex];
    const price = parseInt(selectedLaptop.price);
    const title = selectedLaptop.title;
    const stock = parseInt(selectedLaptop.stock);

    if(stock > 0)
    {
        if(bankAcc.getBalance() >= price)
        {
            alert(`You just bought ${title}!`);
            bankAcc.withdraw(price);
            updateLaptopStock(selectedLaptop);
            reload();
        }
        else
        {
            alert("You do not have enough money");
        }
    }
    else
    {
        alert("Product is out of stock");
    }
}

laptopsElement.addEventListener("change", laptopMenuHandler);
buyElement.addEventListener("click", handleBuyButton);
reload();

//reloads all variables on the page
function reload()
{
    document.getElementById("name").innerHTML = bankAcc.getName();
    document.getElementById("balance").innerHTML = bankAcc.getBalance();
    document.getElementById("salary").innerHTML = workAcc.getSalary();

    // if not on loan, hide the outstanding loan tag
    if(bankAcc.isOnLoan())
    {
        document.getElementById("outstanding-loan").innerHTML = `Of which loaned: ${bankAcc.getLoanAmount()}`;
        if(!onLoan)
        {
            createPayButton();
        }
    }
    else
    {
        document.getElementById("outstanding-loan").innerHTML = "";
        if(onLoan)
        {
            removePayButton();
        }
    }
}
// Function that is called when there is an outstanding loan.
// Creates a button that gives the option to pay back any outstanding loans.
function createPayButton()
{
    let button = document.createElement("INPUT");
    button.id= "pay-button";
    button.class = "pay-button";
    button.type = "button";
    button.onclick = salaryLoan;
    button.value = "Pay";

    document.getElementById("pay-button-label").appendChild(button);
    onLoan = true;
}
// removes the pay button once there is no outstanding loans
function removePayButton()
{
    let elem = document.getElementById("pay-button");
    elem.parentNode.removeChild(elem);
    onLoan = false;
}
// handles loaning an amount
function loan()
{
    try
    {   
        //Check whether there already is an outstanding loan
        if(bankAcc.isOnLoan())
            throw new Error("There already is an outstanding loan");
        else
        {
            //Prompt user to input an amount
            let amount = prompt("Please enter an amount", 0);
            //It should throw an error upon invalid input
            if(!isNaN(amount))
            {
                bankAcc.loan(amount);          
                document.getElementById("outstanding-loan").innerHTML = `Of which loaned: ${amount}`;
            }
            else throw new Error("Invalid value input");
        }
    }
    catch(err)
    {
        //Display the error as an prompt window message
        alert(`Cannot make a loan:\nReason: ${err.message}`)
    }

    // console.log(`Your balance is: ${bankAcc.getBalance()}. Outstanding loans: ${bankAcc.getLoanAmount()}`);
    reload();
}
    /*
        Deposits salary to bank account and takes off a portion if there is an outstanding loan
        (Handled in depositSalary())

        Sanity check: Upon using part of your salary, the new balance will end up as:

                            (balance + loan) + 0.8 * salary

        This is because part of the salary will be used to pay off your outstanding loan
        Because the outstanding loan is added to your balance, it should get reduced twice.

        ex.: balance = 200
            loan    = 100
            salary  = 100

            total balance before salary deposit = 200 + 100 = 300
            salary put aside for loan           = 100 * 0.1 = 10
            salary that goes to balance         = 90, so new balance = 200 + 90 = 290
            salary that goes to loan            = 10, so new loan = 100 - 10 = 90
            new total balance                   = 290 + 90 = 380
    */
function depositSalary()
{
    let amount = workAcc.getSalary();
    let finalAmt = workAcc.depositSalary(amount);
    
    // console.log
    // (
    //     `Deposited ${finalAmt} to Bank. Your balance is: ${bankAcc.getBalance()}. Outstanding loans: ${bankAcc.getLoanAmount()}`
    // );
    reload();
}
// function when clicking the Pay button
function salaryLoan()
{
    let amount = workAcc.getSalary();
    bankAcc.payLoan(amount);
    workAcc.totalSalary = 0;
    // console.log
    // (
    //     `Used full salary to pay off loans. Your balance is: ${bankAcc.getBalance()}. Outstanding loans: ${bankAcc.getLoanAmount()}`
    // );
    reload();
}
//function upon clicking the work button
function work()
{
    workAcc.work()
    reload();
}
//This is for testing purposes only. Use this function in the browser terminal.
function withdraw(amount)
{
    let amt = Number(amount);
    bankAcc.withdraw(amt);
    reload();
}
//This is for testing purposes only. Use this function in the browser terminal.
function deposit(amount)
{
    let amt = Number(amount);
    bankAcc.deposit(amt);
    reload();
}
